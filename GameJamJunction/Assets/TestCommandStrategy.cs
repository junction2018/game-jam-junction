using System.Collections.Generic;
using IBM.Watson.DeveloperCloud.Logging;

public class TestCommandStrategy : Strategy
{
    
    public override PhraseResult getResult(Dictionary<string, WordType> keywords, string input)
    {
        string result = string.Empty;

        foreach (var item in keywords)
        {
            if (input.Contains(item.Key))
                result = item.Value.Type;
        }
        Log.Debug("Obtain Key", string.Format("result = {0}", result));
        return new PhraseResult(result);
    }
}