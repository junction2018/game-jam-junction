﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterFolowwer : MonoBehaviour {

    [SerializeField]
    public Transform target;

    Vector3 offset;

	// Use this for initialization
	void Start () {
        offset = gameObject.transform.position - target.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.position = target.position + offset;
        //gameObject.transform.Translate(0, 0, -10);
	}
}
