


public class WordType
{
    private string _type;

    public WordType(string type)
    {
        _type = type;
    }

    public string Type
    {
        get { return _type; }
    }
}