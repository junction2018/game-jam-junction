public class PhraseResult
{
    private string _direction;
    
    public PhraseResult(string direction)
    {
        _direction = direction;
    }

    public string Direction
    {
        get { return _direction; }
    }
}