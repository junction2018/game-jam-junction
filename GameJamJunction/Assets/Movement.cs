﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICharacterControls
{
    Action Forward { get; set; }
    Action Stop { get; set; }
    Action Left { get; set; }
    Action Right { get; set; }
}

public abstract class IInputSystem : MonoBehaviour
{
    public abstract void REGME(ICharacterControls controls);
}

public class Movement : MonoBehaviour, ICharacterControls
{
    private static float EPS = 0.0001f;
    private static int NOT_STRAFING = 0;
    private static int STRAFING_LEFT = 1;
    private static int STRAFING_RIGHT = 2;

    public Action Forward { get; set; }
    public Action Stop { get; set; }
    public Action Left { get; set; }
    public Action Right { get; set; }


    [SerializeField]
    IInputSystem input;

    [SerializeField]
    Transform leftBoundTransform;

    [SerializeField]
    Transform rightBoundTransform;

    [SerializeField]
    float speedStep;

    [SerializeField]
    float maxSpeed;

    [SerializeField]
    float minSpeed;

    [SerializeField]
    float strafeAcceleration;

    [SerializeField]
    float maxStrafeSpeed;

    [SerializeField]
    public float curSpeed;

    private float leftBoundaryX;
    private float rightBoundaryX;

    private float targetSpeed;
    private float strafeVelocity = 0;
    private float curStrafeAcceleration = 0;
    private float currentAngle;

    private int strafeState = NOT_STRAFING;

    // Use this for initialization
    void Start () {
        Forward = OnForward;
        Stop = OnStop;
        Left = OnLeft;
        Right = OnRight;

        input.REGME(this);

        leftBoundaryX = leftBoundTransform.position.x;
        rightBoundaryX = rightBoundTransform.position.x;

        curSpeed = minSpeed;
        targetSpeed = minSpeed;
	}

    void OnForward()
    {
        targetSpeed += speedStep;
        targetSpeed = Math.Min(targetSpeed, maxSpeed);
    }

    void OnStop()
    {
        targetSpeed = 0;
        targetSpeed = Math.Max(targetSpeed, minSpeed);
    }

    void OnLeft()
    {
        if (strafeState == STRAFING_RIGHT)
        {
            UpdateStrafe(NOT_STRAFING);
            return;
        }
        if (strafeState == NOT_STRAFING)
        {
            UpdateStrafe(STRAFING_LEFT);
        }
    }

    void OnTriggerEnter (Collider col)
    {
        if (col.gameObject.CompareTag("Obstacle"))
        {
            Debug.Log("collision detected");
            OnObstacle();
            if (col.gameObject.GetComponent<CarMovement>() != null)
            {
                gameObject.GetComponent<PlayMakerFSM>().Fsm.Event("Collide_Moving");
            }
            else
            {            	
                gameObject.GetComponent<PlayMakerFSM>().Fsm.Event("Collide_Static");
            }
        }
    }

    void AdjustRotation()
    {
        float angle = (float) Math.Atan(strafeVelocity / curSpeed);

        if (curSpeed < EPS) angle = 0;
        if (Math.Abs(strafeVelocity) < 0.01) angle = 0;

        transform.rotation = Quaternion.identity;
        currentAngle = currentAngle * 0.9f + angle * 0.1f;
        transform.Rotate(Vector3.up, currentAngle / (float) Math.PI * 180);
    }

    void OnObstacle()
    {
        curSpeed = 0;
        targetSpeed = 0;
    }

    void OnRight()
    {
        if (strafeState == STRAFING_LEFT)
        {
            UpdateStrafe(NOT_STRAFING);
            return;
        }
        if (strafeState == NOT_STRAFING)
        {
            UpdateStrafe(STRAFING_RIGHT);
        }
    }

    void Update()
    {
        //CheckZBoundary();
        ProcessMovingForward();
        ProcessSpeed();
        ProcessStrafe();
        CheckStrafeBoundaries();
        AdjustRotation();
    }

    void UpdateStrafe(int newState)
    {
        if (newState == NOT_STRAFING)
        {
            Debug.Log("NOT_STRAFING");
            curStrafeAcceleration = 0;
        }
        if (newState == STRAFING_RIGHT)
        {
            Debug.Log("STRAFING_RIGHT");
            curStrafeAcceleration = strafeAcceleration;
        }
        if (newState == STRAFING_LEFT)
        {
            Debug.Log("STRAFING_LEFT");
            curStrafeAcceleration = -strafeAcceleration;
        }
        strafeState = newState;
    }

    void CheckZBoundary()
    {
        //for debug purposes
        if (transform.position.z > 50)
        {
            Vector3 pos = transform.position;
            transform.position = new Vector3(pos.x, pos.y, 0);
        }
    }

    void CheckStrafeBoundaries()
    {
        float curX = transform.position.x;
        float targetX = 0;
        bool boundaryReached = false;
        Vector3 pos = transform.position;
        if (curX > rightBoundaryX)
        {
            if (strafeState == STRAFING_RIGHT)
            {
                UpdateStrafe(NOT_STRAFING);
            }
            targetX = rightBoundaryX;
            boundaryReached = true;
        }
        if (curX < leftBoundaryX)
        {
            if (strafeState == STRAFING_LEFT)
            {
                UpdateStrafe(NOT_STRAFING);
            }
            targetX = leftBoundaryX;
            boundaryReached = true;
        }
        if (boundaryReached)
        {
            float nextX = targetX * 0.05f + curX * 0.95f;
            transform.position = new Vector3(nextX, pos.y, pos.z);
        }
    }

    void ProcessSpeed()
    {
        curSpeed = targetSpeed * 0.1f + curSpeed * 0.9f;
        if (Math.Abs(curSpeed - targetSpeed) < EPS)
        {
            curSpeed = targetSpeed;
        }
    }

    void ProcessStrafe()
    {
        float acc = curStrafeAcceleration;
        if (strafeState == NOT_STRAFING)
        {
            if (strafeVelocity > 0)
            {
                acc = -strafeAcceleration;
            }
            else
            {
                acc = strafeAcceleration;
            }
        }
        strafeVelocity += acc * Time.deltaTime;
        if (Math.Abs(strafeVelocity) + EPS > maxStrafeSpeed)
        {
            if (strafeVelocity < 0)
            {
                strafeVelocity = -maxStrafeSpeed;
            }
            else
            {
                strafeVelocity = maxStrafeSpeed;
            }
        }
        transform.Translate(strafeVelocity * Time.deltaTime, 0, 0);
    }

    void ProcessMovingForward()
    {
        float dt = Time.deltaTime;
        transform.Translate(0, 0, curSpeed * dt);
    }
}
