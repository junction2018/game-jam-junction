﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputSystem : IInputSystem
{
    ICharacterControls controls;

    public override void REGME(ICharacterControls controls)
    {
        this.controls = controls;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            controls.Forward();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            controls.Stop();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            controls.Left();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            controls.Right();
        }
    }
}