using HutongGames.PlayMaker;
using IBM.Watson.DeveloperCloud.Logging;
using UnityEngine;
using UnityEngine.UI;

public class TextUIScript : MonoBehaviour
{

    [SerializeField]
    private SpeechCommandSystem _speechRecognitionSystem;

    [SerializeField] 
    private SpeechInputSystem _speechInputSystem;

    private string currentState = "";

    private PlayMakerFSM fsm;
    private Text text;

    void Start()
    {
        text = GetComponent<Text>();
        fsm = GetComponent<PlayMakerFSM>();
        _speechRecognitionSystem.newMessageRecieved += handle;
        _speechInputSystem.newMessageFinished += handle;
    }

    // FORGIVE ME FOR THIS CRAP
    private void handle(string input)
    {
        // Log.Debug("TestingLogs", string.Format("Content {0}", input));
        if (input.Equals("good"))
            fsm.SendEvent("Positive");
        else if (input.Equals("bad"))
            fsm.SendEvent("Negative");
        else
        {
            text.text = input;
            fsm.SendEvent("Listening");
        }
    }
}