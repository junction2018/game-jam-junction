﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour {
    private bool movementStarted = false;

    [SerializeField]
    float maxVelocity;

    [SerializeField]
    float acceleration;

    [SerializeField]
    GameObject go;

    //[SerializeField]
    GameObject player;

    float velocity = 0;
    float velocityMultiplier;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (movementStarted)
        {
            if (Mathf.Abs(velocity) < maxVelocity)
            {
                velocity += acceleration * Time.deltaTime;
            }

            transform.Translate(0, 0, velocity * velocityMultiplier * Time.deltaTime);
        }
	}

    float GetPlayerSpeed()
    {
        return player.GetComponent<Movement>().curSpeed;
    }

    public void StartMoving()
    {
        go.SetActive(true);
        movementStarted = true;
        velocityMultiplier = GetPlayerSpeed() / 2;
    }
}
