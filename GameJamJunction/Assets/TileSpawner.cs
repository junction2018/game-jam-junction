﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSpawner : MonoBehaviour {

    [SerializeField]
    Transform direction;

    [SerializeField]
    GameObject[] tilePrefabs;
    
    [SerializeField]
    float tileSize;

    [SerializeField]
    int prespawnCount;

    [SerializeField]
    float distance;

    [SerializeField]
    GameObject tracker;

    [SerializeField]
    float unspawnDistance;
        
    Vector3 lastPoint;
    System.Random random;

    List<GameObject> spawned;

    void Start ()
    {
        spawned = new List<GameObject>();
        random = new System.Random();
        lastPoint = tracker.transform.position;
        for (int i = 0; i < prespawnCount; i++)
        {
            Spawn();
        }
	}
	
	void Update()
    {
        CheckSpawn();
        CheckUnspawn();
	}

    void CheckSpawn()
    {
        if (Vector3.Distance(tracker.transform.position, lastPoint) < distance)
        {
            Spawn();
        }
    }

    void CheckUnspawn()
    {
        var pendingRemove = new List<GameObject>();

        foreach (var item in spawned)
        {
            if(Vector3.Distance(item.transform.position, tracker.transform.position) > unspawnDistance)
            {
                pendingRemove.Add(item);
            }
        }

        foreach (var item in pendingRemove)
        {
            spawned.Remove(item);
            Destroy(item);
        }
    }

    void Spawn()
    {
        var prefab = tilePrefabs[random.Next(tilePrefabs.Length)];
        var tile = Instantiate(prefab, lastPoint, prefab.transform.rotation);
        tile.transform.parent = transform.parent;
        lastPoint += direction.forward.normalized * tileSize;
        spawned.Add(tile);
    }
}
