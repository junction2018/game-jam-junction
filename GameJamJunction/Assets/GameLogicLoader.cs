﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogicLoader : MonoBehaviour {

    [SerializeField]
    GameObject logicPrefab;

    GameObject cachedPrefab;

	void OnEnable () {
        if(cachedPrefab != null)
        {
            Destroy(cachedPrefab);
        }
        cachedPrefab = Instantiate(logicPrefab);
	}
	
}
