using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObstacleSpawner : MonoBehaviour {

    [SerializeField]
    GameObject[] obstaclePrefabs;

    [SerializeField]
    Transform left;

    [SerializeField]
    Transform right;

    [SerializeField]
    GameObject tracker;

    [SerializeField]
    float minDistanceToSpawn;
    
    [SerializeField]
    float maxDistanceToSpawn;

    [SerializeField]
    int preloadCount;

    [SerializeField]
    float unspawnDistance;

    Vector3 lastPoint;
    System.Random rnd;
    List<GameObject> spawnedObstacles;
    List<GameObject> unspawnedObstacles;

    void Start () {
        rnd = new System.Random();
        lastPoint = (left.position + right.position) / 2;
        spawnedObstacles = new List<GameObject>();
        unspawnedObstacles = new List<GameObject>();
        for (int i = 0; i < preloadCount; i++)
        {
            Spawn();
        }
	}

	void Update () {
        CheckObstacles();
        CheckUnspawn();
	}

    void CheckUnspawn()
    {
        List<GameObject> pendingRemove = new List<GameObject>();

        foreach (var item in unspawnedObstacles)
        {
            if(Vector3.Distance(item.transform.position, tracker.transform.position) > unspawnDistance)
            {
                pendingRemove.Add(item);
            }
        }

        foreach (var item in pendingRemove)
        {
            unspawnedObstacles.Remove(item);
            Destroy(item);
        }
    }

    void CheckObstacles()
    {
        List<GameObject> pendingRemove = new List<GameObject>();

        foreach (var item in spawnedObstacles)
        {
            var obsDir = item.transform.position - tracker.transform.position;
            if (Vector3.Dot(left.forward, obsDir) < 0)
            {
                pendingRemove.Add(item);
            }
        }

        foreach (var item in pendingRemove)
        {
            spawnedObstacles.Remove(item);
            unspawnedObstacles.Add(item);
            Spawn();
        }
    }

    void Spawn()
    {
        float rndDistance = (minDistanceToSpawn + (float)rnd.NextDouble() * (maxDistanceToSpawn - minDistanceToSpawn));
        var position = lastPoint + left.forward.normalized * rndDistance;
        if (rnd.Next(2) == 0)
        {
            position += (left.position - right.position) / 2;
        }
        else
        {
            position -= (left.position - right.position) / 2;
        }
        var cols = Physics.OverlapSphere(position, 1);

        bool y = false;

        foreach (var collider in cols)
        {
            if(collider.gameObject.tag == "Road")
            {
                var x = collider.ClosestPoint(position + left.forward.normalized * 7);
                position = left.forward.normalized * 1 + x;
                lastPoint = left.forward.normalized * 1 + x;
                y = true;
                break;
            }
        }
        var prefab = obstaclePrefabs[rnd.Next(obstaclePrefabs.Length)];
        var item = Instantiate(prefab, position, prefab.transform.rotation);
        item.transform.parent = transform.parent;
        if (y) item.GetComponentsInChildren<Renderer>().All((e) => e.material = null);
        spawnedObstacles.Add(item);
        lastPoint += left.forward.normalized * rndDistance;
    }
}
