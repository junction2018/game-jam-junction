using System.Collections.Generic;
using UnityEngine;

public abstract class Strategy : MonoBehaviour
{
    public abstract PhraseResult getResult(Dictionary<string, WordType> keywords, string input);
}