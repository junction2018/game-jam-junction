﻿using System.Collections;
using System.Collections.Generic;
using IBM.Watson.DeveloperCloud.Logging;
using UnityEngine;

public class SpeechInputSystem : IInputSystem
{
	[SerializeField]
	private SpeechCommandSystem speechRecognitionSystem;

	[SerializeField]
	private Strategy strategy;

	private ICharacterControls controls;
	
	private Dictionary<string, WordType> keywords;

	[SerializeField]
	private PlayMakerFSM fsm;

	public System.Action<string> newMessageFinished;

	// Use this for initialization
	void Start ()
	{
		initKeywords();
		speechRecognitionSystem.newMessageRecieved += handle;
		// speechRecognitionSystem.newMessageFinished += handle;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	

	public override void REGME(ICharacterControls controls)
	{
		this.controls = controls;
	}

	private void handle(string input)
	{
		PhraseResult result = strategy.getResult(keywords, input);
		if (result.Direction.Equals("forward"))
		{
			fsm.SendEvent("forward");
			// newMessageFinished("good");
			controls.Forward();	
		}
		if (result.Direction.Equals("left"))
		{
			fsm.SendEvent("left");
			// newMessageFinished("good");
			controls.Left();	
		}
		if (result.Direction.Equals("right"))
		{
			fsm.SendEvent("right");
			// newMessageFinished("good");
			controls.Right();	
		}
		if (result.Direction.Equals("stop"))
		{
			fsm.SendEvent("stop");
			controls.Stop();	
		}
	}

	private void initKeywords()
	{
		keywords = new Dictionary<string, WordType>();
        
		// Good
		keywords.Add("please", new WordType("forward"));
		keywords.Add("boy", new WordType("forward"));
		keywords.Add("go", new WordType("forward"));
		keywords.Add("move", new WordType("forward"));
		keywords.Add("walk", new WordType("forward"));
		keywords.Add("crawl", new WordType("forward"));
		keywords.Add("moving", new WordType("forward"));
		keywords.Add("faster", new WordType("forward"));
		keywords.Add("fast", new WordType("forward"));
		keywords.Add("speed", new WordType("forward"));
		keywords.Add("speedy", new WordType("forward"));
		keywords.Add("straight", new WordType("forward"));
		keywords.Add("forward", new WordType("forward"));
        
		// Bad
		keywords.Add("stop", new WordType("stop"));
		keywords.Add("no", new WordType("stop"));
		keywords.Add("don't", new WordType("stop"));
		keywords.Add("slower", new WordType("stop"));
		keywords.Add("slow", new WordType("stop"));
		keywords.Add("halt", new WordType("stop"));
		

		keywords.Add("left", new WordType("left"));
		keywords.Add("west", new WordType("left"));
		keywords.Add("down", new WordType("left"));
		keywords.Add("downward", new WordType("left"));
		
		keywords.Add("right", new WordType("right"));
		keywords.Add("east", new WordType("right"));
		keywords.Add("up", new WordType("right"));
		keywords.Add("upward", new WordType("right"));
	}
}
